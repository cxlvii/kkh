/** KA-KA-HA.COM Starts here
 *
 */

var kkh = {};


kkh.init = function(){

  var context = this;

  // UI classes
  this.classes = {
    triggerClass: '.js-screen-btn',
    phoneticText: '.js-lang-phonetic',
    langText: '.js-lang-text',
    debug: '.js-debug'
  };

  // Display Objects
  this.DO = {
    $soundTrigger: $(this.classes.triggerClass),
    $phoneticText: $(this.classes.phoneticText),
    $langText: $(this.classes.langText),
    $debug: $(this.classes.debug)
  };

  // Is touch device
  this.isTouch = 'ontouchstart' in document.documentElement;

  // Sound files
  this.soundsPath = '/sounds/';
  this.soundsFiles = [{
      "url" : "kkh_afri.mp3",
      "name" : "Afrikaans",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_alb.mp3",
      "name" : "Albanian",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_arab.mp3",
      "name" : "Arabic",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_armen.mp3",
      "name" : "Armenian",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_bengal.mp3",
      "name" : "Bengali",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_bosn.mp3",
      "name" : "Bosnian",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_cekh.mp3",
      "name" : "Czech",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_china.mp3",
      "name" : "Chinese",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_danish.mp3",
      "name" : "Danish",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_en.mp3",
      "name" : "English",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_espaniol.mp3",
      "name" : "Spanish",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_esperanto.mp3",
      "name" : "Esperanto",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_fins.mp3",
      "name" : "Finnish",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_french.mp3",
      "name" : "French",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_german.mp3",
      "name" : "German",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_grec.mp3",
      "name" : "Greek",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_hindi.mp3",
      "name" : "Hindi",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_horvat.mp3",
      "name" : "Croatian",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_indonez.mp3",
      "name" : "Indonesian",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_islandic.mp3",
      "name" : "Icelandic",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_jap.mp3",
      "name" : "Japanese",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_kambodj.mp3",
      "name" : "Khmer",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_katalansk.mp3",
      "name" : "Catalan",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_korea.mp3",
      "name" : "Korean",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_latinska.mp3",
      "name" : "Latin",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_latvian.mp3",
      "name" : "Latvian",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_makedon.mp3",
      "name" : "Macedonian",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_niderland.mp3",
      "name" : "Dutch",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_norveg.mp3",
      "name" : "Norwegian",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_polish.mp3",
      "name" : "Polish",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_portug.mp3",
      "name" : "Portuguese",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_rumunska.mp3",
      "name" : "Romanian",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_rus.mp3",
      "name" : "Russian",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_serb.mp3",
      "name" : "Serbian",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_singalska.mp3",
      "name" : "Sinhala",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_slovatska.mp3",
      "name" : "Slovak",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_suahili.mp3",
      "name" : "Swahili",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_swed.mp3",
      "name" : "Swedish",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_tai.mp3",
      "name" : "Thai",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_tamilska.mp3",
      "name" : "Tamil",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_turk.mp3",
      "name" : "Turkish",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_ukr.mp3",
      "name" : "Ukrainian",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_valiysk.mp3",
      "name" : "Welsh",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_venger.mp3",
      "name" : "Hungarian",
      "transcription" : "[ KA KA HA ]"
    },{
      "url" : "kkh_viet.mp3",
      "name" : "Vietnamese",
      "transcription" : "[ KA KA HA ]"
    }];

  this.masterVolume = 100;
  this.sounds = [];
  this.soundManagerUsed = false;

  // Detect Available Engine
  this.canUseSSA = 'speechSynthesis' in window;
  this.canUseAU = window.AudioContext || window.webkitAudioContext || false;
  this.soundEngineUsed = "none";
  this.isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;

  if ( this.canUseAU && !this.isFirefox ) {

    this.soundEngineUsed = "Web Audio";

    // Create audioContext
    this.audioCtx = new (window.AudioContext || window.webkitAudioContext)();
    // Sounds array
    this.setEvents();

  } else if ( this.canUseSSA && !this.isFirefox ) {

    this.soundEngineUsed = "Speech Synthesis";

    this.SSAlangs = [];
    window.speechSynthesis.onvoiceschanged = function(e) {
      kkh.getSSALangs();
    };
    this.setEvents2();

  } else {

    this.soundEngineUsed = "SoundManager";
    this.canUseSSA = false;
    this.canUseAU = false;
    this.initSoundManager();

  }

  this.debug();
};


kkh.debug = function(e){

  this.DO.$debug.html( "<span>Can use Speech Synthesis API:</span> " + ( this.canUseSSA ) + "<br>" +
                       "<span>Can use Web Audio API:</span> " + ( this.canUseAU !== false ) + "<br>" +
                       "<span>SoundManager2 used:</span> " + ( this.soundManagerUsed ) + "<br>" +
                       "<span>Sound engine used:</span> " + ( this.soundEngineUsed ) + "<br>" +
                       "<span>Is touch device:</span> " + ( this.isTouch ) );

}


kkh.loadSoundFile = function( _url, callBack ){
  var request = new XMLHttpRequest()
  request.open('GET', _url, true);
  request.responseType = 'arraybuffer';
  request.onload = callBack;
  request.send();
};


kkh.playRandomSound = function(e){

  var context = this,
      rndNum = Math.floor( Math.random() * context.soundsFiles.length );


  // trigger visualisation related event
  $( context ).trigger('sound:request', {ctx: context.audioCtx});

  // Prepare a request
  var fileUrl = context.soundsPath + context.soundsFiles[rndNum].url;

  context.loadSoundFile( fileUrl, function(){
    if ( this.status !== 404){
      context.audioCtx.decodeAudioData(this.response, function(buffer) {

        var source = context.audioCtx.createBufferSource();
        source.buffer = buffer;
        // trigger visualisation related event
        $( context ).trigger('sound:onload', { bufferSrc: source });

        source.connect(context.audioCtx.destination);
        source.loop = false;
        source.start(0);
        // Show text
        var curentLang = context.soundsFiles[rndNum];
        context.showCurrentLang( curentLang.transcription, curentLang.name );
      }, function(e) {
        console.log(e);
      });
    } else {
      // Remove sound file url from array if it's a 404
      context.soundsFiles.splice( rndNum, 1);
      // Call playRandomSound again
      context.playRandomSound();
    }
  });

};


kkh.getSSALangs = function( ){
  var voices = speechSynthesis.getVoices();

  // If not enough voices init SoundManager
  if ( voices.length < 4 ) {
    console.log('Not enough voices');
    this.DO.$soundTrigger.off('click');
    this.DO.$soundTrigger.off('mouseover');

    this.canUseSSA = false;
    this.canUseAU = false;
    this.initSoundManager();
    return false;
  }

  voices.forEach(function(voice, i) {
    this.SSAlangs.push( voice );
  }, this);

};



kkh.playRandomSound2 = function( ){

  var msg = new SpeechSynthesisUtterance(),
      rndNum = Math.floor( Math.random() * this.SSAlangs.length );
      // console.log(rndNum);
  msg.text = 'ka ka ha';
  // console.log(msg);
  msg.volume = this.masterVolume;
  msg.voiceURI = this.SSAlangs[rndNum].voiceURI;
  msg.lang = this.SSAlangs[rndNum].lang;
  if( msg.voiceURI !== "native" ){

    window.speechSynthesis.speak( msg );
    var langDetails = this.getLangDetails( msg );
    this.showCurrentLang( '[ KA KA HA ]', langDetails );

  } else {
    this.playRandomSound2();
  }
  // console.log(msg);
};


kkh.playSM2sound = function(){
  var context = this,
      rndNum = Math.floor( Math.random() * this.soundsFiles.length );


    soundManager.createSound({
      url: context.soundsPath + context.soundsFiles[rndNum].url,
      autoLoad: true,
      autoPlay: true,
      onload: function( isFound ) {
        context.showCurrentLang( '[ KA KA HA ]', context.soundsFiles[rndNum].name );
        if ( !isFound ) {
          // If file not found
          console.log(this.url, ' file not found');
          // Remove url from array
          context.soundsFiles.splice( rndNum, 1);
          // Call playSM2sound again
          context.playSM2sound();
        }
      },
      volume: context.masterVolume
    });

};


kkh.initSoundManager = function(){
  var context = this;
  console.log('Last argument init');
  if ( typeof soundManager !== "object" ){
    // Stop if soundManager is undefined
    throw new Error('soundManager is not loaded');

  } else {
    this.soundManagerUsed = true;
    // Initial soundManager settings and onReady callback
    soundManager.setup({
      url: '/js/plugins/',
      flashVersion: 9,
      preferFlash: false,
      onready: function() {
        // Init rest of code when soundManager have been initialized
        context.setSM2Events();
      },
      ontimeout: function() {
        throw new Error('soundManager could not start. Missing SWF? Flash blocked?');
      }
    });
  }

};


kkh.setEvents = function(){

  if( this.isTouch ) {
    this.DO.$soundTrigger.on('click', $.proxy(this.playRandomSound, this) );
  } else {
    this.DO.$soundTrigger.on('mouseover', $.proxy(this.playRandomSound, this) );
  }

};


kkh.setEvents2 = function(){

  if( this.isTouch ) {
    this.DO.$soundTrigger.on('click', $.proxy(this.playRandomSound2, this) );
  } else {
    this.DO.$soundTrigger.on('mouseover', $.proxy(this.playRandomSound2, this) );
  }

};


kkh.setSM2Events = function(){

  if( this.isTouch ) {
    this.DO.$soundTrigger.on('click', $.proxy(this.playSM2sound, this) );
  } else {
    this.DO.$soundTrigger.on('mouseover', $.proxy(this.playSM2sound, this) );
  }

};


kkh.getLangDetails = function( lang ){

  var str = '';
  str += lang.voiceURI.replace("Google", "").replace("Male", "").replace("Female", "");
  return str;

};


kkh.showCurrentLang = function( phonetic, lang ){

  if ( typeof phonetic !== "undefined" && typeof lang !== "undefined" ){
    TweenLite.to(this.DO.$phoneticText, .4, {text: phonetic, ease:Linear.easeNone});
    TweenLite.to(this.DO.$langText, .4, {text: lang, ease:Linear.easeNone});
  }


};


kkh.init();
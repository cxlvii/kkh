/** KA-KA-HA.COM Visualization starts here
 *
 */
kkh.visualsCnvs = {};


kkh.visualsCnvs.initVoiseVisuals = function() {

  var context = this;
  context.$cnvs = $('#oscillo');
  context.cnvsSettings = {
    width: context.$cnvs.width(),
    height: context.$cnvs.height(),
    strokeColor: 'rgb(200, 200, 200)',
    strokeWidth: 1
  };
  context.analyser = {};
  context.cnvsCtx = context.$cnvs[0].getContext("2d");
  context.$cnvs[0].width = context.cnvsSettings.width;
  context.$cnvs[0].height = context.cnvsSettings.height;

  context.animationStyleStep = 1;
  context.animationStyleStepCount = 3;

  // Getting AudioContext object from main app through custom event
  $( kkh ).on('sound:request', function(e, data){
    // Create audio analyser
    context.analyser = data.ctx.createAnalyser();
    context.analyser.fftSize = 2048;
  });


  // Getting source object from createBufferSource in main app
  $( kkh ).on('sound:onload', function(e, data){
    // Trigger visualization when sound is loaded
    context.play(e, data);
  });

};


kkh.visualsCnvs.play = function( e, data ){

  var _this = this;

  if( data.bufferSrc.buffer ){
    data.bufferSrc.connect( _this.analyser );
  } else {
    throw new Error("Buffer error");
  }

  // Delete previous listener
  data.bufferSrc.onended = null;

  var bufferLength = _this.analyser.frequencyBinCount,
      dataArray = new Uint8Array( bufferLength ),
      requestFrame;


  function drawCanvas() {

    requestFrame = requestAnimationFrame( function(){
      drawCanvas();
    });

    _this.drawOscillo( dataArray, bufferLength );
    _this.animateSVG( dataArray, bufferLength );

  };

  drawCanvas();

  data.bufferSrc.onended = function() {
    _this.stop( requestFrame, data.bufferSrc, _this.cnvsCtx);
  };

};


kkh.visualsCnvs.animateSVG = function ( dataArray, bufferLength ) {

  for( var i = 0; i < 6; i++ ) {

    var _scale = dataArray[i] / 100.0;

    var _color = dataArray[i] / 100.0,
        r = 0,
        g = 255,
        b = 0;

    // Lets pretend that everything > 1 is == to percents
    var modifier = dataArray[i] % 100.0,
        eq = 255 / 100;


    if ( modifier < 50 ) {
      r = Math.round( modifier * eq );
      g = 255;
    } else {
      r = 255;
      g = Math.round( 255 - ( modifier * eq ) );
      b = 0;
    }
    // var _color = 'rgb(0,0,0)';
    var _color = 'rgb(' + Math.abs(r) + ',' + Math.abs(g) + ',' + Math.abs(b) +')';

    var animationPreset = {};

    if( modifier > 38 ){
      // animationStyleStep
      // animationStyleStepCount
      switch ( this.animationStyleStep ) {
        case 1:
          animationPreset = {
            scale: _scale,
            rotation: ( Math.random() < 0.5 ) ? modifier/2 : -modifier/2,
            fill: _color,
            transformOrigin: "50% 50%"
          };
        break;
        case 2:
          animationPreset = {
            y: '+='+_scale,
            scale: _scale,
            fill: _color,
            transformOrigin: "50% 50%"
          };
        break;
        case 3:
          animationPreset = {
            rotationY:  ( Math.random() < 0.5 ) ? modifier : -modifier,
            scale: _scale,
            y: ( Math.random() < 0.5 ) ? '+=' + _scale*2 : '-=' + _scale*2,
            opacity: 1.0 - ( modifier * 0.01 ),
            transformOrigin: "50% 50% 200"
          };
        break;
      }

      TweenMax.to($('svg#kakaha_logo path:nth(' + i + ')'), .1, animationPreset);


    }
  }

};


kkh.visualsCnvs.drawOscillo = function ( dataArray, bufferLength ) {

  this.cnvsCtx.clearRect(0, 0, this.cnvsSettings.width, this.cnvsSettings.height);
  this.cnvsCtx.lineWidth = this.cnvsSettings.strokeWidth;
  this.cnvsCtx.strokeStyle = this.cnvsSettings.strokeColor;

  this.cnvsCtx.beginPath();
  this.cnvsCtx.moveTo(0, this.cnvsSettings.height / 2);

  this.analyser.getByteTimeDomainData(dataArray);

  var sliceWidth = this.cnvsSettings.width * 1.0 / (bufferLength / 4),
      x = 0,
      y = 0,
      midY = this.cnvsSettings.height / 2,
      v = 0;

  for( var i = 0; i < bufferLength; i++ ) {

    v = dataArray[i] / 128.0;
    y = v * midY;

    this.cnvsCtx.lineTo(x, y);

    x += sliceWidth;
  }

  this.cnvsCtx.stroke();
};



kkh.visualsCnvs.stop = function( animationReq, bufferSrc, canvasCtx ){
  var context = this;
  cancelAnimationFrame( animationReq );
  bufferSrc.disconnect();

  TweenMax.to($('svg#kakaha_logo path'), .1, {
    scale: 1,
    fill:'rgb(34,34,34)',
    rotation: 0,
    rotationY: 0,
    y: 0,
    x: 0,
    opacity: 1,
    transformOrigin: "50% 50%",
    onComplete: function(){
      context.animationStyleStep++;
      context.animationStyleStep = ( context.animationStyleStep > context.animationStyleStepCount ) ? 1 : context.animationStyleStep;
    }
  });
  // canvasCtx.clearRect( 0, 0, this.cnvsSettings.width, this.cnvsSettings.height );
};





kkh.visualsCnvs.initVoiseVisuals();
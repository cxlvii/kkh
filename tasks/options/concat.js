module.exports = {
  dist: {
    options: {
      sourceMap: true
    },
    src: [
      'src/js/plugins/jquery.min.js',
      'src/js/plugins/soundmanager2-nodebug-jsmin.js',
      'src/js/plugins/gsap/TweenMax.min.js',
      'src/js/plugins/gsap/TextPlugin.min.js',
      'src/js/app.js',
      'src/js/appVisual.js'
    ],
    dest: 'build/js/app.js'
  }
}
module.exports = {
  server: {
    options: {
      hostname: '127.0.0.1',
      // hostname: '0.0.0.0',
      port: 8000,
      livereload: true,
      base: './build',
      open: {
        target: 'http://127.0.0.1:8000',
      }
    }
  }
}
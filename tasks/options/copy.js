module.exports = {
  images: {
    expand: true,
    cwd: 'src/i/',
    src: ['**/*.{png,jpg,svg}'],
    dest: 'build/i/'
  },
  sounds: {
    expand: true,
    cwd: 'src/sounds/',
    src: ['**/*.{mp3,ogg}'],
    dest: 'build/sounds/'
  }
}